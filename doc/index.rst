#######################
Web Shop Shopify Module
#######################

The *Web Shop Shopify Module* provides a way to manage `Shopify
<https://www.shopify.com/>` stores.
It uploads products, variants and collections to Shopify, and downloads orders,
transactions and creates fulfillments.

.. toctree::
   :maxdepth: 2

   usage
   configuration
   design
